import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessegeSenderComponent } from './messege-sender.component';

describe('MessegeSenderComponent', () => {
  let component: MessegeSenderComponent;
  let fixture: ComponentFixture<MessegeSenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessegeSenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessegeSenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
