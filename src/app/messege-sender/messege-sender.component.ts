import { Component, OnInit } from '@angular/core';
import { SubjectService, Messages } from '../subject.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-messege-sender',
  templateUrl: './messege-sender.component.html',
  styleUrls: ['./messege-sender.component.css'],
})
export class MessegeSenderComponent implements OnInit {
  constructor(private _subject: SubjectService, private _form: FormBuilder) {}
  firstValue: Messages = { sender: null, msg: null, isImportant: null };

  get Input() {
    return this.RegistrationForm.get('Input');
  }
  get IsImportant() {
    return this.RegistrationForm.get('IsImportant');
  }
  RegistrationForm = this._form.group({
    Input: [''],
    IsImportant: [false],
  });

  ngOnInit(): void {
    this._subject.sub.subscribe((x) => (this.firstValue = x));
  }

  emit() {
    if (this.Input.value != '' && this.Input.value != null) {
      this._subject.sendData({
        isImportant: this.IsImportant.value,
        msg: this.Input.value,
        sender: 'Person 1',
      });
      this.RegistrationForm.reset();
    } else {
      alert('please give Input value');
    }
  }
}
