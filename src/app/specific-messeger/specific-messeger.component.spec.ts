import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificMessegerComponent } from './specific-messeger.component';

describe('SpecificMessegerComponent', () => {
  let component: SpecificMessegerComponent;
  let fixture: ComponentFixture<SpecificMessegerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificMessegerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificMessegerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
