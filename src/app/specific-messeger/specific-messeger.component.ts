import { Component, OnInit } from '@angular/core';
import { Messages, SubjectService } from '../subject.service';

@Component({
  selector: 'app-specific-messeger',
  templateUrl: './specific-messeger.component.html',
  styleUrls: ['./specific-messeger.component.css']
})
export class SpecificMessegerComponent implements OnInit {

  constructor(private _subject: SubjectService) { }

  specificMessages: Messages[] = []

  ngOnInit() {
    this._subject.sub.subscribe(x => {
      if (x.isImportant) {
        this.specificMessages.push(x)
      }
    })
  }
}