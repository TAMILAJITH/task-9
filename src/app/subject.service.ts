import { Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SubjectService {
  constructor() {}
  sub: Subject<Messages> = new Subject<Messages>();

  sendData(data: Messages) {
    this.sub.next(data);
  }

  checkData(data) {
    this.sub.next(data);
  }
}

export interface Messages {
  sender: string;
  msg: string;
  isImportant: boolean;
}
