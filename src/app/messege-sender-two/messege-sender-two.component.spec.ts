import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessegeSenderTwoComponent } from './messege-sender-two.component';

describe('MessegeSenderTwoComponent', () => {
  let component: MessegeSenderTwoComponent;
  let fixture: ComponentFixture<MessegeSenderTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessegeSenderTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessegeSenderTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
