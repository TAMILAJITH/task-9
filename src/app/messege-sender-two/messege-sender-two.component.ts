import { Component, OnInit } from '@angular/core';
import { SubjectService, Messages } from '../subject.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-messege-sender-two',
  templateUrl: './messege-sender-two.component.html',
  styleUrls: ['./messege-sender-two.component.css'],
})
export class MessegeSenderTwoComponent implements OnInit {
  constructor(private _subject: SubjectService, private _form: FormBuilder) {}

  secondValue: Messages = { sender: null, msg: null, isImportant: null };
  get Input() {
    return this.RegistrationForm.get('Input');
  }
  get IsImportant() {
    return this.RegistrationForm.get('IsImportant');
  }
  RegistrationForm = this._form.group({
    Input: [''],
    IsImportant: [false],
  });

  ngOnInit(): void {
    this._subject.sub.subscribe((x) => (this.secondValue = x));
  }

  emit() {
    if (this.Input.value != '' && this.Input.value != null) {
      this._subject.sendData({
        isImportant: this.IsImportant.value,
        msg: this.Input.value,
        sender: 'Person 2',
      });
      this.RegistrationForm.reset();
    } else {
      alert('please give Input value');
    }
  }
}
