import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MessegeSenderComponent } from './messege-sender/messege-sender.component';
import { MessegeSenderTwoComponent } from './messege-sender-two/messege-sender-two.component';
import { AllMessegersComponent } from './all-messegers/all-messegers.component';
import { SpecificMessegerComponent } from './specific-messeger/specific-messeger.component';
import { SubjectService } from './subject.service';
import { ReactiveFormsModule } from '@angular/forms';
import { PersonComponent } from './person/person.component'

@NgModule({
  declarations: [
    AppComponent,
    MessegeSenderComponent,
    MessegeSenderTwoComponent,
    AllMessegersComponent,
    SpecificMessegerComponent,
    PersonComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  providers: [SubjectService],
  bootstrap: [AppComponent]
})
export class AppModule { }
