import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMessegersComponent } from './all-messegers.component';

describe('AllMessegersComponent', () => {
  let component: AllMessegersComponent;
  let fixture: ComponentFixture<AllMessegersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMessegersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMessegersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
