import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SubjectService, Messages } from '../subject.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
@Component({
  selector: 'app-all-messegers',
  templateUrl: './all-messegers.component.html',
  styleUrls: ['./all-messegers.component.css']
})
export class AllMessegersComponent implements OnInit {

  constructor(private _subject: SubjectService) { }
  allMessages: Messages[] = [];
  ngOnInit(): void {
    this._subject.sub.subscribe((x) => { this.allMessages.push(x) })
  }

}
