import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SubjectService, Messages } from '../subject.service';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css'],
})
export class PersonComponent implements OnInit {
  secondValue: Messages = { isImportant: null, sender: null, msg: null };

  constructor(private _Form: FormBuilder, private _subject: SubjectService) {}

  get Input() {
    return this.RegistrationForm.get('Input');
  }
  get checkBox() {
    return this.RegistrationForm.get('checkBox');
  }
  RegistrationForm = this._Form.group({
    Input: [''],
    checkBox: [false],
  });

  ngOnInit(): void {
    this._subject.sub.subscribe((x) => (this.secondValue = x));
  }
  sendValue() {
    if (this.Input.value != '' && this.Input.value != null) {
      this._subject.sendData({
        isImportant: this.checkBox.value,
        msg: this.Input.value,
        sender: 'Person 3',
      });
      this.RegistrationForm.reset();
    } else {
      alert('please give Input value');
    }
  }
}
